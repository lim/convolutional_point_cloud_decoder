# A Convolutional Decoder for Point Clouds

This repository contains the code for the paper "A Convolutional Decoder for Point Clouds using Adaptive Instance Normalization"

    @article{Lim:2019:ConvolutionalDecoder,
        author   = "Lim, Isaak and Ibing, Moritz and Kobbelt, Leif",
        title    = "A Convolutional Decoder for Point Clouds using Adaptive Instance Normalization",
        journal  = "Computer Graphics Forum",
        volume   = 38,
        number   = 5,
        year     = 2019
    } 

The paper as well as further data can be found at the [project page][page]

## Usage

We used our own sampling of the [Shapenet][shapenet] Dataset, which can be downloaded under this [link][sampling].

To evaluate a model run

    python test_autoencoder.py --model-path pretrained/model_full_transformations.state \
                               --data-path "data/shapenet_v2_16000.hdf5" \ 
                               --split-path "data/core_train_val_test.csv"

Pretrained weights for our models trained with 3 or 9 layers of adaptive instance normalization can be found under models/pretrained/.

When evaluating the version with only 3 used layers add the option --adain-layer 3 to train a new model run 

    python train_autoencoder.py --run-name full \
                                --data-path "data/shapenet_v2_16000.hdf5" \
                                --split-path "data/core_train_val_test.csv"

The weights will be saved under models/, whereas the progress will be saved under runs/.

There are further command line options to specify the used losses etc.

[shapenet]: https://www.shapenet.org/
[sampling]: https://www.graphics.rwth-aachen.de/blockstorage/free/shapenet_v2_16000.zip
[page]: https://graphics.rwth-aachen.de/publication/03303
