import argparse
from collections import OrderedDict

import numpy as np
import torch
from tqdm import tqdm

import data
import models
from dists import chamfer


def undo_normalize(points, mean, scale):
    res = points / scale.unsqueeze(1).unsqueeze(1)
    res = res + mean.unsqueeze(2).expand_as(points)

    return res


def normalize_unit_cube(points):
    bb_max = points.max(-1)[0]
    bb_min = points.min(-1)[0]
    length = (bb_max - bb_min).max()
    mean = (bb_max + bb_min) / 2.0
    scale = 1.0 / length
    res = (points - mean.unsqueeze(1)) * scale
    return res.clamp(-0.5, 0.5), mean, scale


def normalize_batch(points):
    mean = []
    scale = []
    res = []
    for i in range(points.shape[0]):
        out = normalize_unit_cube(points[i])
        mean.append(out[1])
        scale.append(out[2])
        res.append(out[0])

    return torch.stack(res), torch.stack(mean), torch.stack(scale)


def test(opt):
    test_data = data.Shapenet(opt.data_path, opt.split_path, mode='test')
    test_dataloader = torch.utils.data.DataLoader(test_data,
                                                  batch_size=opt.batch_size,
                                                  shuffle=False,
                                                  drop_last=False)
    device = torch.device('cpu') if opt.no_cuda else torch.device('cuda')

    net = models.GridAutoEncoderAdaIN(rnd_dim=2,
                                      enc_p=opt.enc_dropout,
                                      dec_p=opt.dec_dropout,
                                      adain_layer=opt.adain_layer).to(device)

    total_params = 0
    for param in net.parameters():
        total_params += np.prod(param.size())
    print("Network parameters: {}".format(total_params))
    state_dict = torch.load(opt.model_path)
    new_state_dict = OrderedDict()
    changed = False
    for k, v in state_dict.items():
        if k[:7] == "module.":
            changed = True
            new_state_dict[k[7:]] = v
        else:
            new_state_dict[k] = v
    if changed:
        state_dict = new_state_dict
    net.load_state_dict(state_dict)
    net.eval()

    with torch.no_grad():
        chamfer_per_class = torch.zeros(test_data.n_classes()).to(device)
        sample_per_class = torch.zeros(test_data.n_classes()).to(device)
        chamfer_sum = 0
        sample_sum = 0
        loader = tqdm(test_dataloader, dynamic_ncols=True)
        handled_i = 0
        for batchdata in loader:
            target = batchdata[0][:, :, :opt.n_out_points]
            labels = batchdata[1].to(device)
            inp = batchdata[0][:, :, :opt.n_in_points]
            inp = inp.to(device)
            target = target.to(device)

            inp, mean, scale = normalize_batch(inp)

            pred, _, _, _ = net(inp, opt.n_out_points, not opt.random_sampling)

            pred = undo_normalize(pred, mean, scale)  # all networks should be trained on normalized data

            handled_i += inp.shape[0]

            chamfer_loss = chamfer(pred, target) * 1e3

            chamfer_sum += chamfer_loss.sum().item()
            sample_sum += inp.shape[0]

            chamfer_per_class.index_add_(0, labels, chamfer_loss)
            sample_per_class.index_add_(0, labels, torch.ones_like(chamfer_loss))

            loader.set_description("Chamfer Loss: {:.{prec}f}".format(chamfer_sum / float(sample_sum), prec=5))

    chamfer_avg = float(chamfer_sum) / float(sample_sum)
    avg_per_class = chamfer_per_class / sample_per_class.float()
    print("chamfer distance: {}".format(chamfer_avg))
    print(avg_per_class)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model-path')
    parser.add_argument('--data-path')
    parser.add_argument('--split-path')
    parser.add_argument('--batch-size', type=int, default=32)
    parser.add_argument('--n-in-points', type=int, default=2500)
    parser.add_argument('--n-out-points', type=int, default=2500)
    parser.add_argument('--enc-dropout', type=float, default=0)
    parser.add_argument('--dec-dropout', type=float, default=0.2)
    parser.add_argument('--adain-layer', type=int, default=None)
    parser.add_argument('--random-sampling', action='store_true')
    parser.add_argument('--no-cuda', action='store_true')
    parser.add_argument('--no-cls', action='store_true')
    args = parser.parse_args()

    print(args)
    test(args)
