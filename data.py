import h5py
import numpy as np
import pandas as pd
import torch.utils.data


class Shapenet(torch.utils.data.Dataset):
    def __init__(self, filename, split_file, mode='train', normalization=None, transform=None):
        super().__init__()

        assert mode in ['train', 'val', 'test', 'complete'], "mode '%s' is not supported" % mode

        self.mode = mode
        self.transform = transform

        if mode in ['train', 'val', 'test']:
            split = pd.read_csv(split_file)
            ids = split.loc[split['split'] == mode]['modelId']
            ids = set(ids)

            with h5py.File(filename, "r") as f:
                indices = []
                for i, x in enumerate(f['model_id'][:].tolist()):
                    if x in ids:
                        indices.append(i)
                self.data = torch.from_numpy(f['points'][indices])
                self.label = f['synset_id'][indices]
        else:
            with h5py.File(filename, "r") as f:
                self.data = torch.from_numpy(f['points'][:])
                self.label = f['synset_id'][:]

        label_dict = {key: value for (value, key) in enumerate(np.unique(self.label))}
        self.label = torch.from_numpy(np.vectorize(label_dict.get)(self.label))

        if normalization is not None:
            self.data = torch.stack([normalization(self.data[i]) for i in range(self.data.shape[0])])

        self.cls_count = torch.zeros(self.label.max().item() + 1, dtype=torch.int)
        for i in range(self.label.max().item() + 1):
            self.cls_count[i] = (self.label == i).sum().int()

        self.model_ids = torch.tensor(range(self.data.shape[0]))

    def __getitem__(self, index):

        out = self.data[self.model_ids[index]]
        cls_label = self.label[self.model_ids[index]]

        if self.transform is not None:
            out = self.transform(out)

        return out, cls_label

    def __len__(self):
        return self.model_ids.shape[0]

    def n_classes(self):
        return self.label.max().item() + 1

    def restrict_class(self, classes):
        if not classes:
            self.model_ids = torch.tensor(range(self.data.shape[0]))
        else:
            self.model_ids = torch.cat([(self.label == i).nonzero() for i in classes], dim=0).squeeze(1)


def normalize(points):
    result = points - torch.mean(points, dim=1).unsqueeze(1).expand_as(points)
    rad = torch.max(torch.norm(result, dim=0))
    return result / rad


def normalize_unit_cube(points):
    bb_max = points.max(-1)[0]
    bb_min = points.min(-1)[0]
    length = (bb_max - bb_min).max()
    mean = (bb_max + bb_min) / 2.0
    scale = 1.0 / length
    res = (points - mean.unsqueeze(1)) * scale
    return res.clamp(-0.5, 0.5)


def subsample(points, n=1024):
    return points[:, :n]


def random_subsample(points, n=1024):
    perm = torch.randperm(points.shape[1])[:n]
    return torch.index_select(points, 1, perm)


def random_jitter(points, sigma=0.01, clip=0.05):
    jittered_data = torch.clamp(sigma * torch.randn(points.shape), -1 * clip, clip)
    jittered_data += points
    return jittered_data


def random_scale(points, min_s=0.66, max_s=1.5, anisotrop=False):
    if anisotrop:
        s = torch.FloatTensor(3).uniform_(min_s, max_s).unsqueeze(1).expand_as(points)
    else:
        s = torch.FloatTensor(1).uniform_(min_s, max_s).unsqueeze(1).expand_as(points)
    res = points * s
    return res


def random_translation(points, max_t=0.2):
    translation = torch.FloatTensor(3).uniform_(-max_t, max_t).unsqueeze(1).expand_as(points)
    res = points + translation
    return res


def get_rotation_matrix(angles):
    s = torch.sin(angles)
    c = torch.cos(angles)

    rot_m = torch.zeros(3, 3).to(angles)
    rot_m[0, 0] = c[1] * c[2]
    rot_m[0, 1] = -c[1] * s[2]
    rot_m[0, 2] = s[1]
    rot_m[1, 0] = c[0] * s[2] + c[2] * s[0] * s[1]
    rot_m[1, 1] = c[0] * c[2] - s[0] * s[1] * s[2]
    rot_m[1, 2] = -c[1] * s[0]
    rot_m[2, 0] = s[0] * s[2] - c[0] * c[2] * s[1]
    rot_m[2, 1] = c[2] * s[0] + c[0] * s[1] * s[2]
    rot_m[2, 2] = c[0] * c[1]

    return rot_m


def random_rotation(points, normalize=True):
    center = (points.max(1)[0] + points.min(1)[0]) * 0.5
    angles = torch.rand(3) * 2 * np.pi
    rot_mat = get_rotation_matrix(angles)
    # points are 3 x n
    r_points = rot_mat @ (points - center.unsqueeze(1))
    if normalize:
        r_points = normalize_unit_cube(r_points)

    return r_points


def random_transform(points):
    return random_jitter(random_scale(random_translation(points)))


def batch_augmentation(points, augmentation):
    tList = [augmentation(m) for m in torch.unbind(points, dim=0)]
    res = torch.stack(tList, dim=0)
    return res
